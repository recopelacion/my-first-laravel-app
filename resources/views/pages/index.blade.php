@extends('layouts.app')

@section('content')

    <div class="jumbotron text-center">
        <h1>{{ $title }}</h1>
        <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Omnis, aperiam nemo. Magni, corrupti. Rem delectus eligendi dolores aliquam fugit necessitatibus quam suscipit alias dolorum? Earum culpa reiciendis incidunt quia vero.</p>
        <a href="/login" class="btn btn-primary btn-lg">Login</a> <a href="/register" class="btn btn-success btn-lg">Register</a>
    </div>

@endsection