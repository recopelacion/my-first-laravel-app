@extends('layouts.app')

@section('content')
    <h1>{{ $title }}</h1>
    @if (count( $services ) > 0)
        <ul>
            @foreach ($services as $service)
                <li>{{ $service }}</li>
            @endforeach
        </ul>
    @endif
    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Corporis vitae, blanditiis, nemo provident, quod ex recusandae aperiam optio cum voluptas unde. Debitis dignissimos suscipit repudiandae soluta alias nulla perspiciatis quia.</p>
@endsection