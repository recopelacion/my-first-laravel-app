@extends('layouts.app')

@section('content')

<h1>Create Posts</h1>

<form action="/posts" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="Title">Title</label>
        <input type="text" name="title" id="" class="form-control" placeholder="Title">
    </div>
    <div class="form-group">
        <label for="Title">Body</label>
        <textarea name="body" id="body" cols="30" class="form-control" rows="10" placeholder="Body Text"></textarea>
    </div>
    <div class="form-group">
        <input type="file" name="cover_image" id="cover_image">
    </div>
    <div class="form-group">
        <input type="submit" value="Submit" class="btn btn-primary">
    </div>
</form>
    
@endsection