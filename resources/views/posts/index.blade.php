@extends('layouts.app')

@section('content')

<h1>Posts</h1>

@if (count($posts) > 0)
    @foreach ($posts as $post)
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-3">
                        <div class="img">
                            <img src="/storage/cover_images/{{ $post->cover_image }}" alt=""  style="width:100%">
                        </div>
                    </div>
                    <div class="col-md-9">
                        <h3 class="card-title"><a href="/posts/{{ $post->id }}">{{ $post->title }}</a></h3>
                        <small>Created at: {{ $post->created_at }} by: {{ $post->user->name }}</small>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
    {{ $posts->links() }}
@else
    <p>No Posts Found</p>
@endif
    
@endsection