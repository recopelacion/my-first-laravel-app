@extends('layouts.app')

@section('content')

<h1>Edit Posts</h1>

<form action="/posts/{{ $post->id }}" method="POST" enctype="multipart/form-data">
    <input name="_method" type="hidden" value="PUT">
    @csrf
    <div class="form-group">
        <label for="Title">Title</label>
        <input type="text" name="title" id="" class="form-control" placeholder="Title" value="{{ $post->title }}">
    </div>
    <div class="form-group">
        <label for="Title">Body</label>
        <textarea name="body" id="body" cols="30" class="form-control" rows="10" placeholder="Body Text">{{ $post->body }}</textarea>
    </div>
    <div class="form-group">
        <input type="file" name="cover_image" id="cover_image">
    </div>
    <div class="form-group">
        <input type="submit" value="Submit" class="btn btn-primary">
    </div>
</form>
    
@endsection