@extends('layouts.app')

@section('content')
    <a href="/posts" class="btn btn-default">Go Back</a>
    <h1>{{ $post->title }}</h1>
    <div class="img">
        <img src="/storage/cover_images/{{ $post->cover_image }}" alt="">
    </div>
    <br>
    <div>
        {{ $post->body }}
    </div>
    <hr>
    <small>Written on: {{ $post->created_at }} by: {{ $post->user->name }}</small>
    <hr>

    @if (!Auth::guest())
        @if (Auth::user()->id == $post->user_id)
            <a href="/posts/{{ $post->id }}/edit" class="btn btn-primary">Edit</a>

            <form action="/posts/{{ $post->id }}" method="POST" class="pull-right">
                @csrf
                @method('DELETE')
                <input type="submit" value="Delete" class="btn btn-danger">
            </form>
        @endif
    @endif

@endsection